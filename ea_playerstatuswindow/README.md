Planned changes:

- [x] Better, more readable text font
- [x] Health text improvements (centered, percentage, etc)
- [x] Disable killing spree logic (not implemented in RoR AFAIK)
- [x] Disable mini-morale widget
- [x] Fix portrait renown and carrer arrows when new points are available
- [x] Decouple player buffs/debuffs from the player frame
    - [ ] Move player buffs/debuffs to their own area
- [ ] Improve health & ap bar textures
    - [ ] Add option for AP text in default frame
- [ ] Review right/left click actions on the player frame
