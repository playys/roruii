﻿<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

    <UiMod name="EA_PlayerStatusWindow" version="2.0" date="05/01/2020" >
        <Author name="Play" email="" />
        <Description text="This module contains the EA Default player Player Status window." />
        <Dependencies>        
            <Dependency name="EASystem_Utils" />
            <Dependency name="EASystem_WindowUtils" />
            <Dependency name="EATemplate_DefaultWindowSkin" />
            <Dependency name="EATemplate_UnitFrames" />
            <Dependency name="EA_LegacyTemplates" />
            <Dependency name="EASystem_Tooltips" />
            <Dependency name="EASystem_LayoutEditor" />
            <Dependency name="EA_TacticsWindow" />
        </Dependencies>
        <Files>        
            <File name="PlayerWindow.xml" />
        </Files>
        <OnInitialize>
            <CreateWindow name="PlayerWindow" show="true" />
        </OnInitialize>             
    </UiMod>
    
</ModuleFile>    
