Work done so far:

- [ ] Player and target frames
  - [x] Better, more readable text font
  - [x] Health text improvements (centered, percentage, etc)
  - [x] Disable killing spree logic (not implemented in RoR AFAIK)
  - [x] Disable mini-morale widget
  - [x] Fix portrait renown and carrer notification arrows when new points become available
  - [x] Decouple player buffs/debuffs from the player frame
    - [ ] Move player buffs/debuffs to their own area
  - [x] Improve health & ap bar textures
    - [ ] Add option for AP text in default frame
  - [x] Clean up/replace textures slices that have noise
  - [ ] Purge non-useful right/left click actions on the player frame
  

 
